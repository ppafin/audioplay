  /*
  * audioplay - gstreamer audio player demo
  * Copyright (C) 2021 Pasi Patama, <pasi@patama.net>
  * 
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  * 
  * REF:
  * 
  * hello-world.c by gstreamer 
  * 
  */

#include <gst/gst.h>
#include <glib.h>
#include <sys/types.h>
#include <unistd.h>
#include "log.h"
#include "ini.h"

#define FILENAME_LENGHT		500
#define FILECOUNTMAXIMUM	5000
#define INI_FILE "audioplay.ini"
int g_entrycount=0;
char input_filename[FILECOUNTMAXIMUM][FILENAME_LENGHT];
char input_directoryname[FILECOUNTMAXIMUM][FILENAME_LENGHT];

static gboolean bus_call (GstBus *bus, GstMessage *msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *) data;
  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      log_trace("[%d] End of stream",getpid()); 
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}


static void
on_pad_added (GstElement *element,GstPad *pad, gpointer data)
{
	GstPad *sinkpad;
	GstElement *decoder = (GstElement *) data;
	/* We can now link this pad with the vorbis-decoder sink pad */
	log_trace("[%d] Dynamic pad created, linking demuxer/decoder",getpid());
	sinkpad = gst_element_get_static_pad (decoder, "sink");
	gst_pad_link (pad, sinkpad);
	gst_object_unref (sinkpad);
}


void listfilesindirectory(const char *name, int indent)
{
    DIR *dir;
    struct dirent *fileentry;
    if (!(dir = opendir(name)))
        return;
    while ((fileentry = readdir(dir) ) != NULL ) {
        if (fileentry->d_type == DT_DIR) {
            
        } else {
			sprintf(input_filename[g_entrycount++],"%s/%s",name,fileentry->d_name);
        }   
    }
    closedir(dir);
}

int main (int   argc,char *argv[])
{
	GMainLoop *loop;
	GstElement *pipeline, *source, *demuxer, *decoder, *conv, *sink;
	GstBus *bus;
	guint bus_watch_id;

	/* Monitor file to appear in input directory */
	ini_t *config = ini_load(INI_FILE);
	char *in_directory = "";
	ini_sget(config, "audioplay", "inputdirectory", NULL, &in_directory);
	log_trace("[%d] monitoring input directory (%s) ",getpid(),in_directory);

	/* gstreamer initialisation */
	gst_init (NULL,NULL);
	loop = g_main_loop_new (NULL, FALSE);
	
	while ( 1 )
	{
		/* Monitor input directory for file uploads */
		for (int loop=0; loop<FILECOUNTMAXIMUM;loop++)
		{
			memset(input_filename[loop],0,FILENAME_LENGHT);
			memset(input_directoryname[loop],0,FILENAME_LENGHT);
		}
		g_entrycount=0;
		listfilesindirectory(in_directory,0);
		for (int file_loop=0; file_loop < g_entrycount; file_loop++)
		{ 
				log_trace("[%d] File available: (%s)",getpid(),input_filename[file_loop] );
				
				/* START OF PLAY */
								
				/* gstreamer initialisation 
				gst_init (NULL,NULL);
				loop = g_main_loop_new (NULL, FALSE);*/
				
				/* Create gstreamer elements */
				pipeline = gst_pipeline_new ("audio-player");
				source   = gst_element_factory_make ("filesrc",       "file-source");
				demuxer  = gst_element_factory_make ("oggdemux",      "ogg-demuxer");
				decoder  = gst_element_factory_make ("opusdec",     "vorbis-decoder");
				conv     = gst_element_factory_make ("audioconvert",  "converter");
				sink     = gst_element_factory_make ("autoaudiosink", "audio-output");

				if (!pipeline || !source || !demuxer || !decoder || !conv || !sink) {
				
					log_error("[%d] One element could not be created. Exiting.",getpid());
					return -1;
				}

				/* Set up the pipeline */

				/* we set the input filename to the source element argv[1]   */
				g_object_set (G_OBJECT (source), "location", input_filename[file_loop], NULL);

				/* we add a message handler */
				bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
				bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
				gst_object_unref (bus);

				/* we add all elements into the pipeline */
				/* file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output */
				gst_bin_add_many (GST_BIN (pipeline),
								source, demuxer, decoder, conv, sink, NULL);

				/* we link the elements together */
				/* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
				gst_element_link (source, demuxer);
				gst_element_link_many (decoder, conv, sink, NULL);
				g_signal_connect (demuxer, "pad-added", G_CALLBACK (on_pad_added), decoder);

				/* note that the demuxer will be linked to the decoder dynamically.
				 The reason is that Ogg may contain various streams (for example
				 audio and video). The source pad(s) will be created at run time,
				 by the demuxer when it detects the amount and nature of streams.
				 Therefore we connect a callback function which will be executed
				 when the "pad-added" is emitted.*/

				/* Set the pipeline to "playing" state*/
				log_trace("[%d] Playing: %s",getpid(),input_filename[file_loop]);
				
				gst_element_set_state (pipeline, GST_STATE_PLAYING);

				/* Iterate */
				g_main_loop_run (loop);

				/* Out of the main loop, clean up nicely */
				log_trace("[%d] Returned, stopping playback",getpid());
				gst_element_set_state (pipeline, GST_STATE_NULL);

				/*log_trace("[%d] Deleting pipeline",getpid()); 
				gst_object_unref (GST_OBJECT (pipeline));
				g_source_remove (bus_watch_id);
				g_main_loop_unref (loop);*/
				
				/* END OF PLAY */
  
				/* Remove played file (should we have option for this?) */
				
				if ( remove(input_filename[file_loop]) == 0 ) {
					log_trace("[%d] Removed file (%s) ",getpid(),input_filename[file_loop] );
				} else {
					log_error("[%d] Failed to remove file  (%s).",getpid(),input_filename[file_loop] );
				}	
		}
		sleep(1);
	}
	log_trace("[%d] Deleting pipeline",getpid()); 
	gst_object_unref (GST_OBJECT (pipeline));
	g_source_remove (bus_watch_id);
	g_main_loop_unref (loop); 
				
	return 0;
}

